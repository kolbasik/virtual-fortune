﻿using System;
using System.Web;

namespace VirtualFortune.Web
{
    public static class UserHelper
    {
        private const string UserTokenKey = "__token__";

        public static string GetUserToken(this HttpContextBase context)
        {
            var httpCookie = context.Request.Cookies[UserTokenKey] ?? context.SetUserToken(Guid.NewGuid().ToString());
            return httpCookie.Value;
        }

        public static HttpCookie SetUserToken(this HttpContextBase context, string token)
        {
            var httpCookie = new HttpCookie(UserTokenKey, token) { Expires = DateTime.UtcNow.AddYears(1) };
            context.Response.Cookies.Add(httpCookie);
            return httpCookie;
        }

        public static int GetUserNumber(this HttpContextBase context)
        {
            return GetNumber(context.GetUserToken(), DateTime.Today);
        }
        
        internal static int GetNumber(string userToken, DateTime today)
        {
            return userToken.GetHashCode() ^ today.GetHashCode();
        }
    }
}