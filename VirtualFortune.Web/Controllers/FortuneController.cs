﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using VirtualFortune.Web.Core;
using VirtualFortune.Web.Models;

namespace VirtualFortune.Web.Controllers
{
    public class FortuneController : Controller
    {
        private readonly VirtualFortuneCookieRepository _repository = new VirtualFortuneCookieRepository();

        public ActionResult Index()
        {
            var virtualFortuneCookie = _repository.GetVirtualFortuneCookie(this.HttpContext.GetUserNumber());
            return View(virtualFortuneCookie);
        }

        public async Task<ActionResult> Send(string email)
        {
            if ((email ?? string.Empty).Contains("@"))
            {
                const string subject = "[Fortune]";
                var virtualFortuneCookie = _repository.GetVirtualFortuneCookie(UserHelper.GetNumber(email, DateTime.Today));
                var message = ViewRenderer.Render(ControllerContext, "Email", virtualFortuneCookie);
                await EmailSender.Send("Virtual Fortune Cookie", subject, message, email);
                return View("Index", virtualFortuneCookie);
            }
            return RedirectToAction("Index");
        }
    }
}