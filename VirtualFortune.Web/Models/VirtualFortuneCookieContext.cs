﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Web;

namespace VirtualFortune.Web.Models
{
    // You can add custom code to this file. Changes will not be overwritten.
    // 
    // If you want Entity Framework to drop and regenerate your database
    // automatically whenever you change your model schema, add the following
    // code to the Application_Start method in your Global.asax file.
    // Note: this will destroy and re-create your database with every model change.
    // 
    // System.Data.Entity.Database.SetInitializer(new System.Data.Entity.DropCreateDatabaseIfModelChanges<Spa.Models.TodoItemContext>());
    public class VirtualFortuneCookieContext : DbContext
    {
        static VirtualFortuneCookieContext()
        {
            Database.SetInitializer(new VirtualFortuneCookieDatabaseInitializer());
        }

        public VirtualFortuneCookieContext()
            : base("name=VirtualFortuneCookieConnection")
        {
        }

        public DbSet<VirtualFortuneCookie> VirtualFortuneCookies { get; set; }

        public DbSet<VirtualFortuneCookieForUser> VirtualFortuneCookieForUsers { get; set; }
    }

    public sealed class VirtualFortuneCookieDatabaseInitializer : CreateDatabaseIfNotExists<VirtualFortuneCookieContext>
    {
        protected override void Seed(VirtualFortuneCookieContext context)
        {
            try
            {
                string rootPath = HttpContext.Current.Server.MapPath("~/App_Data/Fortunes");
                foreach (var filePath in Directory.GetFiles(rootPath, "*.txt", SearchOption.TopDirectoryOnly))
                {
                    string[] data = Path.GetFileNameWithoutExtension(filePath).Split('_');
                    var provider = data.ElementAtOrDefault(0) ?? string.Empty;
                    var lang = data.ElementAtOrDefault(1) ?? string.Empty;
                    var type = data.ElementAtOrDefault(2) ?? string.Empty;
                    IEnumerable<string> fortunes = File.ReadAllLines(filePath).SkipWhile(line => line.StartsWith("//")).Select(x => x.Trim()).Distinct();
                    foreach (var fortune in fortunes)
                    {
                        var virtualFortuneCookie = EntityFactory.CreateNew<VirtualFortuneCookie>();
                        virtualFortuneCookie.Fortune = fortune;
                        virtualFortuneCookie.Lang = lang;
                        virtualFortuneCookie.Type = type;
                        virtualFortuneCookie.Provider = provider;
                        context.VirtualFortuneCookies.Add(virtualFortuneCookie);
                    }
                }
                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }
    }
}