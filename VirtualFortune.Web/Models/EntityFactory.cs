﻿using System;

namespace VirtualFortune.Web.Models
{
    public static class EntityFactory
    {
        public static T CreateNew<T>() where T : IEntity, new ()
        {
            return new T() { Id = Guid.NewGuid() };
        }
    }
}