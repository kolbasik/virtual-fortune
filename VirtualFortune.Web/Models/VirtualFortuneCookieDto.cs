﻿namespace VirtualFortune.Web.Models
{
    public class VirtualFortuneCookieDto
    {
        /// <summary>
        /// Data transfer object for <see cref="VirtualFortuneCookie"/>
        /// </summary>
        public VirtualFortuneCookieDto() { }

        public VirtualFortuneCookieDto(VirtualFortuneCookie item)
        {
            Message = item.Fortune;
            Type = item.Type;
            Lang = item.Lang;
        }

        public string Message { get; set; }

        public string Type { get; set; }

        public string Lang { get; set; }
    }
}
