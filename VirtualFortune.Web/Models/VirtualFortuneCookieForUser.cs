﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace VirtualFortune.Web.Models
{
    public sealed class VirtualFortuneCookieForUser : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string UserToken { get; set; }

        [ForeignKey("VirtualFortuneCookie")]
        public Guid VirtualFortuneCookieId { get; set; }

        public VirtualFortuneCookie VirtualFortuneCookie { get; set; }

        public long DateTicks { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}