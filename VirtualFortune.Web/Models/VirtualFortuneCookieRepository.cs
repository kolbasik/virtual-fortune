﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace VirtualFortune.Web.Models
{
    public sealed class VirtualFortuneCookieRepository
    {
        private static readonly List<VirtualFortuneCookie> VirtualFortuneCookies;

        static VirtualFortuneCookieRepository()
        {
            try
            {
                VirtualFortuneCookies = new List<VirtualFortuneCookie>();
                using (var db = new VirtualFortuneCookieContext())
                {
                    VirtualFortuneCookies.AddRange(db.VirtualFortuneCookies);
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.ToString());
            }
        }

        public VirtualFortuneCookie GetVirtualFortuneCookie(int number)
        {
            var position = Math.Abs(number) % VirtualFortuneCookies.Count;
            return VirtualFortuneCookies[position];
        }
    }
}