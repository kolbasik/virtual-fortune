﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VirtualFortune.Web.Models
{
    public sealed class Subscription : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string UserToken { get; set; }

        public string Email { get; set; }

        public string Skype { get; set; }
    }
}