﻿using System;

namespace VirtualFortune.Web.Models
{
    public interface IEntity
    {
        Guid Id { get; set; }
    }
}