﻿using System;
using System.ComponentModel.DataAnnotations;

namespace VirtualFortune.Web.Models
{
    public sealed class VirtualFortuneCookie : IEntity
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Fortune { get; set; }

        public string Type { get; set; }

        public string Lang { get; set; }

        public string Provider { get; set; }
    }
}