﻿using System.IO;
using System.Web.Mvc;

namespace VirtualFortune.Web.Core
{
    public static class ViewRenderer
    {
        public static string Render(ControllerContext controllerContext, string viewName, object model = null)
        {
            using (var writer = new StringWriter())
            {
                var viewData = new ViewDataDictionary { Model = model };
                var tempData = new TempDataDictionary();
                var view = ViewEngines.Engines.FindView(controllerContext, viewName, null);
                var context = new ViewContext(controllerContext, view.View, viewData, tempData, writer);
                view.View.Render(context, writer);
                writer.Flush();
                return writer.ToString();
            }
        }
    }
}