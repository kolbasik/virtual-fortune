﻿using System;
using System.Diagnostics;
using System.Net.Mail;
using System.Threading.Tasks;

namespace VirtualFortune.Web.Core
{
    public static class EmailSender
    {
        public static async Task<bool> Send(string name, string subject, string message, params string[] receivers)
        {
            bool state = false;
            try
            {
                if (receivers.Length > 0)
                {
                    using (var mail = new MailMessage())
                    {
                        foreach (var receiver in receivers)
                        {
                            mail.To.Add(receiver);
                        }

                        mail.From = new MailAddress(mail.From.Address, name);
                        mail.Subject = subject;
                        mail.IsBodyHtml = false;
                        mail.Body = message;

                        using (var smtp = new SmtpClient())
                            await smtp.SendMailAsync(mail);

                        state = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Trace.TraceError(ex.Message);
            }
            return state;
        }
    }
}