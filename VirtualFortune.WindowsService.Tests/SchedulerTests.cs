﻿using System.Threading;
using NUnit.Framework;

namespace VirtualFortune.WindowsService.Tests
{
    [TestFixture]
    public class SchedulerTests
    {
        [Test]
        public void Start_Alway_RaiseWork()
        {
            // Arrange
            const int interval = 100, timeout = 450, expected = timeout / interval + 1;
            int count = 0;

            var scheduler = new Scheduler(interval, () => count++, exception => { });

            // Act
            scheduler.Start();
            Thread.Sleep(timeout);

            // Assert
            Assert.AreEqual(expected, count);
        }

        [Test]
        public void Stop_Never_RaiseWork()
        {
            // Arrange
            const int interval = 100, timeout = 450, expected = 0;
            int count = 0;

            var scheduler = new Scheduler(interval, () => count++, exception => { });

            // Act
            scheduler.Start();
            scheduler.Stop();
            Thread.Sleep(timeout);

            // Assert
            Assert.AreEqual(expected, count);
        }
    }
}
