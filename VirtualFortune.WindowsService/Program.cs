﻿using System;
using System.ServiceProcess;
using System.Threading;

namespace VirtualFortune.WindowsService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            var servicesToRun = new ServiceBase[] 
                { 
                    new VirtualFortuneService()
                };
            ServiceBase.Run(servicesToRun);
        }
    }
}
