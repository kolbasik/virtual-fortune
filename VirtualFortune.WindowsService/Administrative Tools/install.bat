@ECHO OFF
cd ..
set DOTNETFX=%SystemRoot%\Microsoft.NET\Framework\v4.0.30319\
set PATH=%PATH%;%DOTNETFX%

echo Installing WindowsService...
echo ---------------------------------------------------
InstallUtil /i VirtualFortune.WindowsService.exe
echo ---------------------------------------------------
echo Done.
pause