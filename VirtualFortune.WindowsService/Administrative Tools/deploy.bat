rem http://www.microsoft.com/resources/documentation/windows/xp/all/proddocs/en-us/xcopy.mspx?mfr=true
echo off
set serviceName=VirtualFortuneService
set basePath=%~dp0
set sourcePath="%basePath%.."
set targetPath="C:\Program Files (x86)\Virtual Fortune\"

echo %sourcePath%
echo %targetPath%

net stop %serviceName%

del /F /S /Q %targetPath%
xcopy %sourcePath% %targetPath% /e /y

net start %serviceName%
pause
echo on