﻿using System;
using System.Threading;

namespace VirtualFortune.WindowsService
{
    public sealed class Scheduler
    {
        private const double Interval = 60 * 60 * 1000;

        private Timer _timer;
        private readonly double _interval;
        private readonly Action _onNext;
        private readonly Action<Exception> _onError;

        /// <summary>
        /// This statement is used to set interval to 1 minute (= 60,000 milliseconds)
        /// </summary>
        /// <param name="interval"></param>
        /// <param name="onNext"></param>
        /// <param name="onError"></param>
        public Scheduler(double interval, Action onNext, Action<Exception> onError)
        {
            if (onNext == null) throw new ArgumentNullException("onNext");
            if (onError == null) throw new ArgumentNullException("onError");

            _interval = interval;
            _onNext = onNext;
            _onError = onError;
        }

        public void Start()
        {
            Stop();
            _timer = new Timer(OnElapsedTime, null, TimeSpan.FromMilliseconds(0), TimeSpan.FromMilliseconds(_interval));
        }

        public void Stop()
        {
            if (_timer != null) _timer.Dispose();
            _timer = null;
        }

        private void OnElapsedTime(object source)
        {
            try
            {
                _onNext();
            }
            catch (Exception ex)
            {
                _onError(ex);
            }
        }
    }
}