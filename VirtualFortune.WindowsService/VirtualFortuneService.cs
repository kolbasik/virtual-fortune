﻿using System;
using System.Diagnostics;
using System.Net;
using System.ServiceProcess;

namespace VirtualFortune.WindowsService
{
    public partial class VirtualFortuneService : ServiceBase
    {
        private readonly Scheduler _scheduler;

        public VirtualFortuneService()
        {
            InitializeComponent();

            _scheduler = new Scheduler(Convert.ToDouble(TimeSpan.TicksPerHour / TimeSpan.TicksPerMillisecond), OnNext, OnError);
            //_scheduler = new Scheduler(Convert.ToDouble(TimeSpan.TicksPerMinute / TimeSpan.TicksPerMillisecond), OnNext, OnError);
        }

        protected override void OnStart(string[] args)
        {
            this.EventLog.WriteEntry("starting service");
            _scheduler.Start();
            this.EventLog.WriteEntry("started service");
        }

        protected override void OnStop()
        {
            this.EventLog.WriteEntry("stoping service");
            _scheduler.Stop();
            this.EventLog.WriteEntry("stoped service");
        }

        private void OnNext()
        {
            this.EventLog.WriteEntry("time to work.");
            
            if (DateTime.Now.Hour != 9) return;

            var emails = new[] { "sergei.kolbasin@gmail.com", "yuliyakiro@gmail.com"/*, "Iuliia_Kiro@epam.com"*/ };
            foreach (var email in emails)
            {
                try
                {
                    this.EventLog.WriteEntry("send to " + email + ".");
                    using (var client = new WebClient())
                    {
                        client.DownloadString(new Uri("http://virtual-fortune.azurewebsites.net/fortune/send/?email=" + email));
                    }
                }
                catch (Exception ex)
                {
                    OnError(ex);
                }
            }
            this.EventLog.WriteEntry("time to rest.");
        }

        private void OnError(Exception exception)
        {
            this.EventLog.WriteEntry(exception.ToString(), EventLogEntryType.Error);
        }
    }
}
